#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "task.h"
#include "tasklist.h"

void set_task_file(char *filepath, char *string_id, char *string_status)
{
  struct TaskList *tl = load_list(filepath);
  if (!tl)
    return;

  int id = atoi(string_id);
  int status = atoi(string_status);

  struct Task *t = get_task(tl, id);
  if (!t)
    return;

  t->status = status;
  push_task(tl, t);
  remove_task(tl, id);

  save_list(tl, filepath);

}

void print_task_file(char *filepath, char *string_id)
{
  struct TaskList *tl = load_list(filepath);
  if (!tl)
    return;

  int id = atoi(string_id);
  
  struct Task *t = get_task(tl, id);
  print_a_task(t);
}

void add_task_file(char *filepath, char *string_id, char* title)
{
  struct TaskList *tl = load_list(filepath);
  struct Task *t = create_task(atoi(string_id), title);

  add_to_list(tl, t);
  save_list(tl, filepath);
}

void parse_options(int argc, char *argv[])
{
  (void) argv;

  if (argc == 1)
    return;

  char *filepath = argv[1];

  for (int i = 2; i <= argc; ++i)
  {
    if (!strcmp(argv[i], "set"))
    {
      set_task_file(filepath, argv[i + 1], argv[i + 2]);
      i += 2;
    }
    else if (!strcmp(argv[i], "print"))
    {
      print_task_file(filepath, argv[i + 1]);
      i += 1;
    }
    else if (!strcmp(argv[i], "add"))
    {
      add_task_file(filepath, argv[i + 1], argv[i + 2]);
      i += 2;
    }
    else
    {
      printf("ERROR:\targv[%d] = %s\n", i, argv[i]);
      return;
    }
  }
}
