#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "task.h"

struct Task* create_task(int id, char* title) {
  struct Task* alloca = malloc(sizeof(struct Task));

  if (!alloca)
    return NULL;

  alloca->id = id;
  alloca->title = malloc(MAX_TITLE_SIZE);

  if (!alloca->title)
    return NULL;

  strcpy(alloca->title, title);
  alloca->status = TODO;

  return alloca;
}

void destroy_task(struct Task* task) {
  free(task->title);
  free(task);
}

void print_status(enum STATUS statval) {
  switch (statval) {
    case TODO: printf("TODO"); break;
    case PENDING: printf("PENDING"); break;
    case DONE: printf("DONE"); break;
  }
}

void print_a_task(struct Task* task) {
  printf("%d: %s [", task->id, task->title);
  print_status(task->status);
  printf("]\n");
}
