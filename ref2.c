#include <stdlib.h>
#include <stdio.h>
#include "task.h"
#include "tasklist.h"

struct TaskList* init_list() {
  struct TaskList* alloca = malloc(sizeof(struct TaskList));

  if (!alloca) {
    return NULL;
  }

  alloca->head = NULL;
  alloca->count = 0;

  return alloca;
}

struct TaskListNode* init_node(struct Task* task) {
  struct TaskListNode* taskContainer = malloc(sizeof(struct TaskListNode));

  if (!taskContainer)
    return NULL;

  taskContainer->next = NULL;
  taskContainer->task = task;

  return taskContainer;
}

void free_node(struct TaskListNode* tln) {
  destroy_task(tln->task);
  free(tln);
}

void destroy_list(struct TaskList* tl) {
  if (tl->count != 0) {
    struct TaskListNode* work_Var = tl->head;
    struct TaskListNode* tmp = NULL;
    while (work_Var != NULL) {
      tmp = work_Var->next;
      free_node(work_Var);
      work_Var = tmp;
    }
  }

  free(tl);
}

void add_to_list(struct TaskList* tl, struct Task* task) {
  struct TaskListNode* work_Var = tl->head;
  struct TaskListNode* new_elem = init_node(task);

  tl->count++;

  // Empty list case
  if (!work_Var) {
    tl->head = new_elem;
    return;
  }
  // At least one elem case

  while (work_Var->next != NULL)
    work_Var = work_Var->next;
  work_Var->next = new_elem;
}

void print_all_tasks(struct TaskList* tl) {
  struct TaskListNode* tln = tl->head;

  while (tln) {
    print_a_task(tln->task);
    tln = tln->next;
  }
}
