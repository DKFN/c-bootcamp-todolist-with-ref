#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include "task.h"
#include "tasklist.h"

// Returns void or an int ?
void save_list(struct TaskList *tl, char *path)
{
  (void) tl;

  FILE *save_file = NULL;
  if ((save_file = fopen(path, "w")) == NULL)
    return;

  struct TaskListNode *sentinel = tl->head;
  fprintf(save_file, "%d\n", tl->count);

  while (sentinel)
  {
    fprintf(save_file, "%d;%s;%d\n", sentinel->task->id,
            sentinel->task->title, sentinel->task->status);
    sentinel = sentinel->next;
  }
  fclose(save_file);
}

void get_information(char *line, int *id, char **title, enum STATUS *status)
{
  int i = 0;
  // id
  char *string_id = malloc(sizeof (16));
  char *string_status = malloc(sizeof (16));
  for (; line[i] != ';'; ++i)
    string_id[i] = line[i];
  *id = atoi(string_id);
  i++;

  // title
  for (; line[i] != ';'; ++i)
    *title[i] = line[i];
  i++;

  // status
  for (int j = 0; line[i] != '\n'; ++i, ++j)
    string_status[j] = line[i];
  *status = atoi(string_status);

  free(string_id);
  free(string_status);
}

struct TaskList* load_list(char *path)
{
  (void) path;
  FILE *load_file = NULL;
  if ((load_file = fopen(path, "r")) == NULL)
    return NULL;

  struct TaskList *tl = init_list();
  char *line = NULL;
  size_t bufferlen = 0;
  char *title = malloc(128);

  
  while ((getline(&line, &bufferlen, load_file)) != -1)
  {
    (void) tl;
    struct Task *t = NULL;
    struct TaskListNode *tln= NULL;
    int id = 0;
    enum STATUS status;
    get_information(line, &id, &title, &status);
    
    t = create_task(id, title);
    tln = init_node(t);
    tln->task->status = status;
  }
  free(line);
  free(title);
  fclose(load_file);
  return tl;
}
