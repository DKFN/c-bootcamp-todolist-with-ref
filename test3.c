#include <stdio.h>
#include "task.h"
#include "tasklist.h"


void assert(const char *msg, int condition)
{
    printf("%s: %s\n", msg, condition ? "OK" : "KO");
}

int main()
{
    struct TaskList* tl = init_list();
    struct Task* t1 = create_task(1, "Become a light butterfly");
    struct Task* t2 = create_task(2, "Be under the projectors");

    add_to_list(tl, t1);
    add_to_list(tl, t2);

    assert("Find an existing task (1)", find_a_task(tl, 1) == 0);
    assert("Find an existing task (2)", find_a_task(tl, 2) == 0);
    assert("Find an existing task (3)", find_a_task(tl, 3) == -1);

    assert("Get a task (1)", get_task(tl, 1) == t1);
    assert("Get a task (2)", get_task(tl, 2) == t2);
    assert("Get a task (3)", get_task(tl, 3) == NULL);

    remove_task(tl, 1);
    assert("Remove a task (1)", find_a_task(tl, 1) == -1);
    print_all_tasks(tl);

    remove_task(tl, 3);
    assert("Remove a task (2)", find_a_task(tl, 3) == -1);
    print_all_tasks(tl);

    printf("Push a new task\n");
    push_task(tl, create_task(3, "New pushed task"));
    print_all_tasks(tl);

    printf("Pop the tasklist\n");
    pop_tasklist(tl);
    print_all_tasks(tl);

    destroy_list(tl);

    return 0;
}
