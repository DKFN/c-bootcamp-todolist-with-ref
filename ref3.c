#include <stdlib.h>
#include <stdio.h>
#include "task.h"
#include "tasklist.h"

int find_a_task(struct TaskList *tl, int id)
{
  struct TaskListNode *sentinel = tl->head;

  while (sentinel)
  {
    if (sentinel->task->id == id)
      return 0;
    sentinel = sentinel->next;
  }
  return -1;
}

struct Task* get_task(struct TaskList *tl, int id)
{
  struct TaskListNode *sentinel = tl->head;

  while (sentinel)
  {
    if (sentinel->task->id == id)
      return sentinel->task;
    sentinel = sentinel->next;
  }
  return NULL;
}

void remove_task(struct TaskList *tl ,int id)
{
  struct TaskListNode *sentinel = tl->head;
  struct TaskListNode *previous = NULL;

  while(sentinel)
  {
    if (sentinel->task->id == id)
    {
      if (previous && sentinel->next)
      {
        previous->task = sentinel->next->task;
        previous->next = sentinel->next->next;
      }
      else if (sentinel->next)
      {
        tl->head = sentinel->next;
      }

      destroy_task(sentinel->task);
      free(sentinel);

      tl->count--;
      return;
    }

    previous = sentinel;
    sentinel = sentinel->next;
  }
}

void push_task(struct TaskList *tl, struct Task *task)
{
  struct TaskListNode *new_node = init_node(task);
  struct TaskListNode *sentinel = tl->head;

  if (!tl->head)
      return;

  for (; sentinel->next; sentinel = sentinel->next);

  sentinel->next = new_node;

  tl->count++;
}

void pop_tasklist(struct TaskList *tl)
{
  struct TaskListNode *to_destroy = tl->head;

  if (!tl->head)
      return;

  tl->head = tl->head->next;

  destroy_task(to_destroy->task);
  free(to_destroy);

  tl->count--;
}
