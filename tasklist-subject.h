#ifndef TASKLIST_H
# define TASKLIST_H

/**
 * It will be the list containing all the tasks
 * I suggest you keep the pointer of the first element here
 */
struct TaskList {
  // TODO : Implement
};

/**
 * A node is an element of the list, it has the reference to the next element
 * an the task
 */
struct TaskListNode {
  // TODO : Implement
};

// Inits a note structure containg a Task
struct TaskListNode* init_node(struct Task* task);

// Creates a list and returns its pointer
struct TaskList* init_list();

// Destroys a node
void free_node(struct TaskListNode* tln) {

// Destroy the list and all its elements
void destroy_list(struct TaskList *tl);

// FIXME : Duplicate of push, merge it or keep it on chap 2?
// Adds the given task at the end of the list
void add_to_list(struct TaskList* tl, struct Task* task);

// Removes the task at the given index // FIXME : Duplicate ?
void remove_at_index(struct TaskList* tl, int idx);

// Prints all the tasks from the list
void print_all_tasks(struct TaskList* tl);

#endif
